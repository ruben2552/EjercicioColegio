package main.java.base.aplicacion;

import java.io.IOException;
import java.sql.SQLException;

import main.java.base.colegio.Colegio;

public class App {

	public static void main(String[] args) {
		try {
			Colegio c = new Colegio();

			c.ejecutarPrograma();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

}