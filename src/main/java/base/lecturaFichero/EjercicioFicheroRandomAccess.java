package main.java.base.lecturaFichero;

import java.io.IOException;
import java.io.RandomAccessFile;

public class EjercicioFicheroRandomAccess {
	
	public static void main(String[] args) throws IOException {
		escribirFichero("Ruben.txt", "qwerty", 10);
		
		byte[] lecturaFichero = leerFichero("Ruben.txt", 12, 100);
		String texto = new String(lecturaFichero);
		
		System.out.println(texto);
		
	}
	
	public static byte[] leerFichero(String _nombreFichero, int posicion, int tamanio) throws IOException {
		RandomAccessFile file = new RandomAccessFile(_nombreFichero, "r");
		file.seek(posicion);
		
		byte[] arrayBytes = new byte[tamanio];
		file.read(arrayBytes);
		file.close();
		
		return arrayBytes;
	}
	
	public static void escribirFichero(String _nombreFichero, String _texto, int posicion) throws IOException {
		RandomAccessFile file = new RandomAccessFile(_nombreFichero, "rw");
		file.seek(posicion);
		file.write(_texto.getBytes());
		file.close();
	}
	
}