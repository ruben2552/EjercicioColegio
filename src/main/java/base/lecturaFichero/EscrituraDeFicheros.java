package main.java.base.lecturaFichero;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import main.java.base.colegio.Profesor;

public class EscrituraDeFicheros {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Profesor persona = new Profesor("Ruben", "Juan Molina", "74009793W", "H", 25);
		
		File file = new File("Yomismo.bin");
		//Clase para escribir en fichero
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		
		os.writeObject(persona);
		os.close();
		fos.close();
		
		//Clase para leer desde fichero
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Profesor contenido = (Profesor) ois.readObject();
		ois.close();
		fis.close();
		
		System.out.print(contenido.getNombre());
	}
}