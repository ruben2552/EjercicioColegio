package main.java.base.lecturaFichero;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LecturaFichero {
	public File fichero;
	public FileWriter escribirFichero;
	public FileReader leerFichero;
	
	public void crearFichero(String _nombre) throws IOException {
		fichero = new File(_nombre);
		escribirFichero = new FileWriter(fichero);
		escribirFichero.write("Hola mi nombre es Ruben Juan Molina");
		escribirFichero.close();
	}
	
	public void lecturaFichero(String _nombreFichero) throws IOException {
		leerFichero = new FileReader(_nombreFichero);
		char chr = (char) leerFichero.read();
		
		do {
			System.out.print(chr);
			chr = (char) leerFichero.read();
		} while (chr != -1);
		
	}
	
	public void escribirFicheroBinario() throws FileNotFoundException {
		fichero = new File("FicheroBinario.bin");
		FileOutputStream fous = new FileOutputStream(fichero);
		
	}	
}