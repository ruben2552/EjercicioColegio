package main.java.base.gestionBBDD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import main.java.base.colegio.Alumno;
import main.java.base.colegio.Asignatura;
import main.java.base.colegio.PersonaLectiva;
import main.java.base.colegio.Profesor;
import main.java.base.colegio.Sexo;

public class GestionBaseDeDatosJDBC {

	private final String bd_url = "jdbc:mysql://localhost/colegio";
	private final String usuarioBD = "root";
	private String contrasenaBD;
	private String sql = "";

	public GestionBaseDeDatosJDBC(String _pass) {
		this.contrasenaBD = _pass;
	}
	
	public Connection crearConexion() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(this.bd_url, this.usuarioBD, this.contrasenaBD);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return conn;
	}

	private void cerrarConexion(Connection _conn) throws SQLException {
		if (_conn != null) {
			_conn.close();
		}
	}

	public void insertarEnBBDD(Object _objetoTabla) throws ClassNotFoundException, IOException, SQLException {

		Connection conn = this.crearConexion();
		Statement pstmt = conn.createStatement();
		try {
			if (_objetoTabla instanceof Profesor) {
				Profesor prof = (Profesor) _objetoTabla;

				this.sql = "INSERT INTO profesor VALUES ('" + prof.getNombre() + "', '" + prof.getApellidos() + "', "
						+ "'" + prof.getDni() + "', '" + prof.getSexo().getInicial() + "', " + prof.getEdad() + ")";

				pstmt.executeUpdate(this.sql);

				System.out.println("Profesor insertado en la BBDD");
			} else if (_objetoTabla instanceof Alumno) {
				Alumno alumno = (Alumno) _objetoTabla;
				this.sql = "INSERT INTO alumno VALUES ('" + alumno.getNombre() + "', '" + alumno.getApellidos() + "', "
						+ "'" + alumno.getDni() + "', '" + alumno.getSexo().getInicial() + "', " + alumno.getEdad()
						+ ")";

				pstmt.executeUpdate(this.sql);

				System.out.println("Alumno insertado en la BBDD");
			} else if (_objetoTabla instanceof Asignatura) {
				Asignatura asig = (Asignatura) _objetoTabla;
				this.sql = "INSERT INTO asignatura VALUES ('" + asig.getNombre() + "', '" + asig.getSiglas() + "', "
						+ asig.getNumHorasLectivas() + ")";

				pstmt.executeUpdate(this.sql);

				System.out.println("Asignatura insertada en la BBDD");
			}

		} finally {
			this.cerrarConexion(conn);
		}
	}

	public HashSet<PersonaLectiva> obtenerPersonasBBDD(String _tabla) throws SQLException {
		int contPersonas = 0;
		Connection conn = this.crearConexion();
		Statement stmt = null;
		ResultSet rs = null;
		HashSet<PersonaLectiva> persona = new HashSet<PersonaLectiva>();
		PersonaLectiva p = null;
		this.sql = "SELECT * FROM " + _tabla;
		String obtMas1 = ((_tabla.equals("profesor")) ? (_tabla + "es") : (_tabla + "s"));

		try {
			stmt = conn.createStatement();

			rs = stmt.executeQuery(this.sql);

			while (rs.next()) {
				p = new PersonaLectiva();

				p.setNombre(rs.getString(1));
				p.setApellidos(rs.getString(2));
				p.setDni(rs.getString(3));
				p.setSexo(Sexo.getSexo(rs.getString(4)));

				persona.add(p);
				contPersonas++;
			}

			if (contPersonas == 1) {
				System.out.println("Se ha recuperado " + contPersonas + " " + _tabla);
			} else if (contPersonas > 1){
				System.out.println("Se han recuperado " + contPersonas + " " + obtMas1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (persona.size() == 0) {
				System.out.println("No hay registros de " + obtMas1 + " en la BBDD");
			}
			this.cerrarConexion(conn);
		}
		return persona;
	}

	public HashSet<Asignatura> obtenerAsignaturasBBDD() throws ClassNotFoundException, IOException, SQLException {
		int contAsig = 0;
		HashSet<Asignatura> asignaturas = new HashSet<Asignatura>();
		Asignatura asig = null;
		String sql = "SELECT * FROM asignatura;";
		Connection conn = this.crearConexion();

		try {

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				asig = new Asignatura();
				String nombre = rs.getString(1);
				String siglas = rs.getString(2);
				int horasLectivasAsignatura = rs.getInt(3);

				asig.setNombre(nombre);
				asig.setSiglas(siglas);
				asig.setNumHorasLectivas(horasLectivasAsignatura);

				asignaturas.add(asig);
				contAsig++;
			}

			if (contAsig == 1) {
				System.out.println(contAsig + " Asignatura recuperada");
			} else if (contAsig > 1) {
				System.out.println(contAsig + " Asignaturas recuperadas");
			}
		} finally {
			if (asignaturas.size() == 0) {
				System.out.println("No hay registros de Asignaturas en la BBDD");
			}
			this.cerrarConexion(conn);
		}

		return asignaturas;
	}
}
