package main.java.base.colegio;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"nombre", "apellidos", "dni", "edad", "sexo"})
public abstract class Persona implements Serializable {

	private static final long serialVersionUID = -753619987866332778L;
	
	protected String nombre;
	protected String apellidos;
	protected String dni;
	protected Sexo sexo;
	protected int edad;

	public Persona() {

	}

	public Persona(String _nombre, String _apellidos, String _dni, String _sexo, int _edad) {
		this.nombre = _nombre;
		this.apellidos = _apellidos;
		this.dni = _dni;
		this.sexo = Sexo.getSexo(_sexo);
		this.edad = _edad;
	}

	@XmlElement(name = "nombre")
	public String getNombre() {
		return this.nombre;
	}

	@XmlElement(name = "apellidos")
	public String getApellidos() {
		return this.apellidos;
	}

	@XmlAttribute(name = "dni")
	public String getDni() {
		return this.dni;
	}

	@XmlElement(name = "sexo")
	public Sexo getSexo() {
		return this.sexo;
	}
	
	@XmlElement(name = "edad")
	public int getEdad() {
		return this.edad;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
}