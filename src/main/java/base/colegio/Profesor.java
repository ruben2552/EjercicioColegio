package main.java.base.colegio;

import java.io.Serializable;
import java.util.HashSet;

public class Profesor extends PersonaLectiva implements Serializable {

	private static final long serialVersionUID = 533936818830463136L;
	
	public Profesor() {
		super();
	}
	
	public Profesor(String _nombre, String _apellidos, String _dni, String _sexo, int _edad) {
		super(_nombre, _apellidos, _dni, _sexo, _edad);
		this.asignatura = new HashSet<Asignatura>();
	}
}