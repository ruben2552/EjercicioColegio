package main.java.base.colegio;

import lombok.Getter;
import lombok.Setter;

public enum Sexo {
	HOMBRE,
	MUJER;
	
	@Getter
	@Setter
	private String inicial;
	
	public static Sexo getSexo(String _sexo) {
		Sexo salida = null;
		if (_sexo.equals("H")) {
			salida = Sexo.HOMBRE;
		} else if(_sexo.equals("F")) {
			salida = Sexo.MUJER;
		}
		return salida;
	}
	
	public String getInicial() {
		return this.inicial;
	}
}
