package main.java.base.colegio;

import java.io.Serializable;
import java.util.HashSet;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import main.java.base.colegio.Persona;

public class PersonaLectiva extends Persona implements Serializable{

	private static final long serialVersionUID = 793441949128567327L;
	
	@XmlElementWrapper(name = "asignaturas")
	@XmlElement(name = "asignatura")
	protected HashSet<Asignatura> asignatura;	
	protected int cantidadAsignatura;
	
	public PersonaLectiva() {
		
	}
	
	public PersonaLectiva(String _nombre, String _apellidos, String _dni, String _sexo, int _edad) {
		super(_nombre, _apellidos, _dni, _sexo, _edad);
		this.cantidadAsignatura = 0;
	}
	
	public void insertarAsignatura(Asignatura _asignatura) {
		if (this.asignatura.size() > 0) {
			for (Asignatura asignatura : this.asignatura) {
				if (asignatura == null) {
					this.asignatura.add(_asignatura);
					this.cantidadAsignatura++;
					break;
				} else if (this.asignatura.contains(_asignatura)) {
					System.out.println("La asignatura ya esta vinculada");
				}
			}
		}
	}

	@XmlAttribute(name = "numeroAsignaturas")
	public int getCantidadAsignaturas() {
		return this.asignatura.size();
	}
	
	public String getAsignatura() {
		String datosAsignaturaDevuelta = "";
		
		for (Asignatura asignatura : this.asignatura) {
			if (asignatura != null) {
				datosAsignaturaDevuelta += "\t" + asignatura.getNombre() + "\n\t" + asignatura.getSiglas() + "\n\t" + asignatura.getNumHorasLectivas();
			}
		}
		
		return datosAsignaturaDevuelta;
	}
	
	public void setAsignatura(HashSet<Asignatura> asignatura) {
		this.asignatura = asignatura;
	}

	public void setCantidadAsignaturas(int cantidadAsignatura) {
		this.cantidadAsignatura = cantidadAsignatura;
	}
}