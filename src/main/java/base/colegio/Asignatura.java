package main.java.base.colegio;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Asignatura implements Serializable{
	
	private static final long serialVersionUID = 2715174243385087417L;
	private String nombre;
	private String siglas;
	private int numHorasLectivas;
	
	public Asignatura() {
		
	}
	
	public Asignatura(String _nombre, String _siglas, int _numHorasLectivas) {
		this.nombre = _nombre;
		this.siglas = _siglas;
		this.numHorasLectivas = _numHorasLectivas;
	}
	
	@XmlElement(name = "nombre")
	public String getNombre() {
		return this.nombre;
	}
	
	@XmlElement(name = "numeroHorasLectivas")
	public String getSiglas() {
		return this.siglas;
	}
	
	@XmlAttribute(name = "siglas")
	public int getNumHorasLectivas() {
		return this.numHorasLectivas;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public void setNumHorasLectivas(int numHorasLectivas) {
		this.numHorasLectivas = numHorasLectivas;
	}

}