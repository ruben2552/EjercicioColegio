package main.java.base.colegio;

import java.util.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import main.java.base.gestionBBDD.GestionBaseDeDatosJDBC;

@XmlRootElement
public class Colegio implements Serializable {

	@XmlElementWrapper(name = "profesores")
	@XmlElement(name = "profesor")
	private HashSet<PersonaLectiva> profesor;

	@XmlElementWrapper(name = "alumnos")
	@XmlElement(name = "alumno")
	private HashSet<PersonaLectiva> alumno;

	@XmlElementWrapper(name = "asignaturas")
	@XmlElement(name = "asignatura")
	private HashSet<Asignatura> asignatura;
	protected String[] datosPersona = { "nombre", "apellidos", "dni", "sexo" };
	protected String[] datosAsignatura = { "nombre", "siglas" };
	private int contadorAsignaturas;
	protected int horasLectivasAsignatura;
	protected int edad;
	private int seleccionMenu;
	protected String contrasenaBD;
	
	private Scanner sc;
	private FileInputStream fis;
	private FileOutputStream fos;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private File archivoXML = null;
	private GestionBaseDeDatosJDBC mybbdd;

	private final File[] ficheros = new File[3];
	private final int[] contadorPersonas = new int[2];
	private final int limiteAsignaturas = 20;
	private final int[] limitePersonas = { 10, 20 };
	private final String[] tipoPersona = { "profesor", "alumno" };

	// TODO: HOLA
	public Colegio() throws ClassNotFoundException {
		this.sc = new Scanner(System.in);
		this.ficheros[0] = new File(
				"C:\\Users\\Paloma Iglesias Dome\\Documents\\Ruben\\Programacion Talent Campus\\Profesores.bin");
		this.ficheros[1] = new File(
				"C:\\Users\\Paloma Iglesias Dome\\Documents\\Ruben\\Programacion Talent Campus\\Alumnos.bin");
		this.ficheros[2] = new File(
				"C:\\Users\\Paloma Iglesias Dome\\Documents\\Ruben\\Programacion Talent Campus\\Asignaturas.bin");
		this.archivoXML = new File(
				"C:\\Users\\Paloma Iglesias Dome\\Documents\\Ruben\\Programacion Talent Campus\\Colegio.xml");
		this.contadorPersonas[0] = 0;
		this.contadorPersonas[1] = 0;
		this.contadorAsignaturas = 0;
		this.horasLectivasAsignatura = 0;
		this.asignatura = new HashSet<Asignatura>();
		this.profesor = new HashSet<PersonaLectiva>();
		this.alumno = new HashSet<PersonaLectiva>();

	}

	public void ejecutarPrograma() throws IOException, ClassNotFoundException, SQLException {
		this.cargarDatos();
		this.menu();
	}

	private void crearFicheros() {
		try {
			for (int i = 0; i < 3; i++) {
				if (!this.ficheros[i].exists()) {
					this.ficheros[i].createNewFile();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void cargarDatos() throws ClassNotFoundException, IOException, SQLException {

		System.out.print("1. Cargar datos de Fichero Binario\n2. Cargar datos de Fichero XML\n"
				+ "3. Cargar datos de la Base de Datos\n4. Salir y no cargar Datos\nElige: ");
		this.seleccionMenu = Integer.parseInt(this.sc.nextLine());
		if (this.seleccionMenu == 1) {
			this.cargarDatosFicheroArray();
		} else if (this.seleccionMenu == 2) {
			this.cargarFicheroXmlArray();
		} else if (this.seleccionMenu == 3) {
			this.cargarDatosBBDD();
		} else if (this.seleccionMenu == 4) {
			this.menu();
		}
	}

	protected void menu() throws IOException, ClassNotFoundException, SQLException {
		try {
			System.out.println("\nBIENVENIDO AL GESTOR DE COLEGIOS. ¿Que opcion quieres elegir?");
			System.out.print("1. Crear Asignaturas.\n" + "2. Crear Profesores.\n" + "3. Crear Alumnos\n"
					+ "4. Vincular\n" + "5. Consultar Registros\n" + "6. Guardar Registros en Fichero\n"
					+ "7. Guardar Registros en XML\n8. Guardar en Base de Datos\n9. Salir\nEscribe tu seleccion y pulsa la tecla INTRO -> ");

			this.seleccionMenu = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("La opcion introducida no es correcta. Vuelve a intentarlo");
			this.menu();
		}
		System.out.println();
		this.seleccionarOpcion();
	}

	private void seleccionarOpcion() throws IOException, ClassNotFoundException, SQLException {
		do {
			if (this.seleccionMenu == 1) {
				this.crearAsignatura();
			} else if (this.seleccionMenu == 2) {
				this.crearPersona(0);
			} else if (this.seleccionMenu == 3) {
				this.crearPersona(1);
			} else if (this.seleccionMenu == 4) {
				this.menuVincular();
			} else if (this.seleccionMenu == 5) {
				this.menuConsultarDatos();
			} else if (this.seleccionMenu == 6) {
				this.guardarRegistrosFichero();
			} else if (this.seleccionMenu == 7) {
				this.guardarEnFicheroXML();
			} else if (this.seleccionMenu == 8) {
				this.guardarEnBBDD();
			}
			System.out.println();
			this.menu();
		} while (this.seleccionMenu != 9);
	}

	private void menuVincular() throws IOException, ClassNotFoundException {
		try {
			System.out.print("Selecciona que quieres vincular\n" + "1. Profesores y Asignaturas.\n"
					+ "2. Alumnos y Asignaturas.\n" + "3. Volver al menu anterior.\n"
					+ "Escribe tu seleccion y pulsa la tecla INTRO -> ");

			this.seleccionMenu = Integer.parseInt(sc.nextLine());
			if (this.seleccionMenu <= 0) {
				System.out.println("Opcion incorrecta. Vuelve a intentarlo.");
				this.menuVincular();
			} else if (this.seleccionMenu == 1) {
				this.vincularPersonaAsignatura(0);
				System.out.println();
			} else if (this.seleccionMenu == 2) {
				this.vincularPersonaAsignatura(1);
				System.out.println();
			} else if (this.seleccionMenu == 3) {
				this.menu();
			}
		} catch (NumberFormatException e) {
			System.out.println("La opcion introducida no es correcta. Vuelve a intentarlo.");
			this.menuVincular();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.menuVincular();
	}

	private void menuConsultarDatos() throws IOException, ClassNotFoundException {
		int opcion;
		System.out.print("1. Consultar Profesores\n2. Consultar Alumnos\n"
				+ "3. Consultar Asignaturas\n4. Volver al menu anterior.\nElige una de las opciones: ");
		opcion = Integer.parseInt(sc.nextLine());

		try {
			if (opcion == 1) {
				this.visualizarDatosPersona(opcion, this.profesor);
			} else if (opcion == 2) {
				this.visualizarDatosPersona(opcion, this.alumno);
			} else if (opcion == 3) {
				this.visualizarDatosAsignatura();
			} else if (opcion == 4) {
				this.menu();
			}

		} catch (NumberFormatException e) {
			System.out.println("Opcion introducida incorrecta. Vuelve a intentarlo.");
			this.menuConsultarDatos();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

	}

	private void cargarDatosFicheroArray() throws ClassNotFoundException, IOException, SQLException {
		this.crearFicheros();
		int contador = 0;
		HashSet<PersonaLectiva> profesor = null;
		HashSet<PersonaLectiva> alumno = null;
		HashSet<Asignatura> asignatura = null;
		try {
			while (contador < this.ficheros.length) {
				boolean ficheroVacio = this.ficheros[contador].exists() && this.ficheros[contador].length() == 0;
				if (contador == 0 && !ficheroVacio) {
					this.fis = new FileInputStream(this.ficheros[contador]);
					this.ois = new ObjectInputStream(this.fis);
					profesor = (HashSet<PersonaLectiva>) this.ois.readObject();

					this.profesor.addAll(profesor);
				} else if (contador == 1 && !ficheroVacio) {
					this.fis = new FileInputStream(this.ficheros[contador]);
					this.ois = new ObjectInputStream(this.fis);
					alumno = (HashSet<PersonaLectiva>) this.ois.readObject();

					this.alumno.addAll(alumno);
				} else if (contador == 2 && !ficheroVacio) {
					this.fis = new FileInputStream(this.ficheros[contador]);
					this.ois = new ObjectInputStream(this.fis);
					asignatura = (HashSet<Asignatura>) this.ois.readObject();

					this.asignatura.addAll(asignatura);
				}
				contador++;
			}

			this.fis.close();
			this.ois.close();
			this.menu();

		} catch (NullPointerException e) {
			System.out.println("No hay registros de personas para cargar.\n");
			this.cargarDatos();
		} catch (FileNotFoundException e) {
			System.out.println("No se han encontrado los ficheros para cargar datos arrays.\n");
		} catch (IOException e) {
			e.printStackTrace();
			contador++;
			this.cargarDatosFicheroArray();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
	}

	private void cargarFicheroXmlArray() throws IOException {
		JAXBContext contexto = null;
		Colegio colegio = null;
		try {
			if (!this.archivoXML.exists()) {
				this.archivoXML.createNewFile();
			}

			if (this.archivoXML.length() != 0) {
				contexto = JAXBContext.newInstance(Colegio.class);
				Unmarshaller unmarsaller = contexto.createUnmarshaller();

				colegio = (Colegio) unmarsaller.unmarshal(this.archivoXML);

				if (colegio != null) {
					this.profesor = colegio.profesor;
					this.alumno = colegio.alumno;
					this.asignatura = colegio.asignatura;
				}
			} else {
				System.out.println("No se han cargado datos ya que el archivo esta vacio.");
			}

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	private void cargarDatosBBDD() throws ClassNotFoundException, IOException, SQLException {
		try {
			System.out.print("Contrase�a BBDD: ");
			this.contrasenaBD = this.sc.nextLine();
			this.mybbdd = new GestionBaseDeDatosJDBC(this.contrasenaBD);
			
			System.out.println("\n=====================================");
			this.profesor = this.mybbdd.obtenerPersonasBBDD("profesor");
			this.alumno = this.mybbdd.obtenerPersonasBBDD("alumno");
			this.asignatura = this.mybbdd.obtenerAsignaturasBBDD();
			System.out.println("=====================================");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			this.cargarDatos();
		}
		this.menu();
	}

	private void crearPersona(int _tipoPersona) throws IOException, ClassNotFoundException, SQLException {
		String persona = ((_tipoPersona == 0) ? "Profesor" : "Alumno");
		try {

			System.out.print("Introduce Nombre: ");
			this.datosPersona[0] = sc.nextLine();
			System.out.print("Introduce Apellidos: ");
			this.datosPersona[1] = sc.nextLine();
			System.out.print("Introduce DNI: ");
			this.datosPersona[2] = sc.nextLine();
			System.out.print("Introduce Sexo (H / F): ");
			this.datosPersona[3] = sc.nextLine().toUpperCase();
			System.out.print("Introduce edad: ");
			this.edad = Integer.parseInt(sc.nextLine());

			if (_tipoPersona == 0) {
				Profesor profe = new Profesor(this.datosPersona[0], this.datosPersona[1], this.datosPersona[2],
						this.datosPersona[3], this.edad);

				this.profesor.add(profe);
				this.mybbdd.insertarEnBBDD("profesor");
				this.contadorPersonas[0]++;
			} else if (_tipoPersona == 1) {
				Alumno alumn = new Alumno(this.datosPersona[0], this.datosPersona[1], this.datosPersona[2],
						this.datosPersona[3], this.edad);

				this.alumno.add(alumn);
				this.mybbdd.insertarEnBBDD("alumno");
				this.contadorPersonas[1]++;
			} else {
				System.out.println("El dni del " + this.tipoPersona[_tipoPersona] + " ya esta registrado.");
			}

		} catch (NullPointerException e) {
			System.out.println("El campo sexo no puede estar vacio. Vuelve a intentarlo.");
			System.out.print("Introduce Sexo (H / F): ");
			this.datosPersona[3] = sc.nextLine().toUpperCase();
		} catch (NumberFormatException e) {
			System.out.println(
					"La edad introducida no es correcta. No se ha introducido un numero. Vuelve a intentarlo.");
			System.out.print("Introduce edad: ");
			this.edad = Integer.parseInt(sc.nextLine());
		} catch (Exception e) {
			System.out.println("Error al capturar los datos. Vuelve a intentarlo.");
			this.crearPersona(_tipoPersona);
		}
		this.menu();
	}

	private void crearAsignatura() throws IOException, ClassNotFoundException, SQLException {
		PreparedStatement stmt = null;
		if (this.contadorAsignaturas > this.asignatura.size()) {
			System.out.println("No se pueden crear mas Asignaturas.");
		} else {
			try {
				System.out.println("CREANDO ASIGNATURA...");
				System.out.print("Introduce Nombre: ");
				this.datosAsignatura[0] = sc.nextLine();

				System.out.print("Introduce Siglas (DAM): ");
				this.datosAsignatura[1] = sc.nextLine().toUpperCase();

				System.out.print("Introduce el Numero de horas lectivas: ");
				this.horasLectivasAsignatura = Integer.parseInt(sc.nextLine());

			} catch (EmptyStackException e) {
				System.out.println("No se pueden dejar espacios en blanco.");
				this.crearAsignatura();
			} catch (NumberFormatException e) {
				System.out.println("El numero de horas introducidas no es correcta. Vuelve a intentarlo.");
				this.crearAsignatura();
			}

			Asignatura asignatura = new Asignatura(this.datosAsignatura[0], this.datosAsignatura[1],
					this.horasLectivasAsignatura);

			this.asignatura.add(asignatura);
			this.mybbdd.insertarEnBBDD(asignatura);
			this.contadorAsignaturas++;

			this.menu();
		}
	}

	private void guardarRegistrosFichero() throws ClassNotFoundException, IOException, SQLException {
		try {
			if (this.ficheros[0].exists() && this.contadorPersonas[0] > 0) {
				this.fos = new FileOutputStream(this.ficheros[0]);
				this.oos = new ObjectOutputStream(fos);

				this.oos.writeObject(this.profesor);

				System.out.println(
						this.tipoPersona[0] + " guardado correctamente en " + this.ficheros[0].getName() + "!");
			} else if (this.contadorPersonas[0] == 0) {
				System.out.println("No hay " + this.tipoPersona[0] + " registrados para guardar en Fichero.");
			}

			if (this.ficheros[1].exists() && this.contadorPersonas[1] > 0) {
				this.fos = new FileOutputStream(this.ficheros[1]);
				this.oos = new ObjectOutputStream(fos);

				this.oos.writeObject(this.alumno);

				System.out.println(
						this.tipoPersona[1] + " guardado correctamente en " + this.ficheros[1].getName() + "!");
			} else if (this.contadorPersonas[1] == 0) {
				System.out.println("No hay " + this.tipoPersona[1] + " registrados para guardar en Fichero.");
			}

			if (this.ficheros[2].exists() && this.contadorAsignaturas > 0) {
				this.fos = new FileOutputStream(this.ficheros[2]);
				this.oos = new ObjectOutputStream(fos);

				this.oos.writeObject(this.asignatura);

				System.out.println("Asignatura guardada correctamente en " + this.ficheros[2].getName() + "!");
			} else if (this.contadorAsignaturas == 0) {
				System.out.println("No hay Asignaturas registradas para guardar en Fichero.");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			this.fos.close();
			this.oos.close();
			this.menu();
		}
	}

	private void guardarEnFicheroXML() {
		try {
			if (!this.archivoXML.exists()) {
				this.archivoXML.createNewFile();
			}

			JAXBContext context = JAXBContext.newInstance(Colegio.class);
			Marshaller marshaller = context.createMarshaller();

			marshaller.setProperty(marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(this, archivoXML);

		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void guardarEnBBDD() {
		System.out.print(
				"1. Guardar Profesores en BBDD\n2. Guardar Alumnos en BBDD\n3. Guardar Asignaturas en BBDD\n4. Volver: ");
		this.seleccionMenu = Integer.parseInt(this.sc.nextLine());

		try {
			this.mybbdd = new GestionBaseDeDatosJDBC(this.contrasenaBD);
			do {
				if (this.seleccionMenu < 1 || this.seleccionMenu > 4) {
					System.out.print("Opcion Incorrecta. Vuelve a intentarlo.");
					this.guardarEnBBDD();
				} else if (this.seleccionMenu == 1) {
					this.mybbdd.insertarEnBBDD("profesor");
				} else if (this.seleccionMenu == 2) {
					this.mybbdd.insertarEnBBDD("alumno");
				} else if (this.seleccionMenu == 3) {
					this.mybbdd.insertarEnBBDD("asignatura");
				}
			} while (this.seleccionMenu != 4);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void vincularPersonaAsignatura(int _tipoPersona) throws ClassNotFoundException, IOException {
		PersonaLectiva personaEncontrada = null;
		System.out.print("Introduce el DNI del " + this.tipoPersona[_tipoPersona] + ": ");
		this.datosPersona[2] = sc.nextLine().toUpperCase();
		if (_tipoPersona == 0) {
			personaEncontrada = this.comprobarDni(this.profesor);
		} else if (_tipoPersona == 1) {
			personaEncontrada = this.comprobarDni(this.alumno);
		}

		this.vincularAsignatura(personaEncontrada);
	}

	private void vincularAsignatura(PersonaLectiva persona) throws ClassNotFoundException, IOException {
		System.out.print(
				"Indique las siglas de la asignatura a vincular.\n" + "VOLVER si desea volver al menu anterior: ");
		this.datosAsignatura[1] = this.sc.nextLine().toUpperCase();

		if (!this.datosAsignatura[1].equals("VOLVER")) {
			persona.insertarAsignatura(comprobarSiglas());
		} else {
			this.menuVincular();
		}
	}

	private PersonaLectiva comprobarDni(HashSet<PersonaLectiva> _persona) {
		PersonaLectiva personaEncontrada = null;

		// DatosPersona[2] = dni

		for (PersonaLectiva personaLectiva : _persona) {
			if (personaLectiva != null && this.datosPersona[2].equals(personaLectiva.getDni())) {
				personaEncontrada = personaLectiva;
				break;
			}
		}
		return personaEncontrada;
	}

	public Asignatura comprobarSiglas() {
		Asignatura asignaturaEncontrada = null;

		for (Asignatura asignatura : this.asignatura) {
			if (asignatura != null && asignatura.getSiglas().equals(this.datosAsignatura[1])) {
				asignaturaEncontrada = asignatura;
				break;
			}
		}

		return asignaturaEncontrada;
	}

	private void visualizarDatosPersona(int _opcion, HashSet<PersonaLectiva> _listaRecibida) throws ClassNotFoundException, IOException {
		String tipoPersona = ((_opcion == 1) ? "profesores" : "alumnos");

		System.out.println("\n=============================");
		if (_listaRecibida.size() != 0 && (_opcion == 1 || _opcion == 2)) {
			for (PersonaLectiva persona : _listaRecibida) {
				System.out.println(persona.getNombre());
				System.out.println(persona.getApellidos());
				System.out.println(persona.getDni());
				System.out.println(persona.getSexo());

				if (!persona.getAsignatura().isEmpty()) {
					System.out.println(persona.getAsignatura());
				}
			}
		} else {
			System.out.println("No hay " + tipoPersona + " almacenados para mostrar.");
		}
		System.out.println("=============================\n");
		this.menuConsultarDatos();
	}
	
	private void visualizarDatosAsignatura() throws ClassNotFoundException, IOException {
		System.out.println("\n=============================");
		if (this.asignatura.size() != 0) {
			for (Asignatura asigRecibida : this.asignatura) {
				Asignatura asignatura = (Asignatura) asigRecibida;

				System.out.println(asignatura.getNombre());
				System.out.println(asignatura.getSiglas());
				System.out.println(asignatura.getNumHorasLectivas());
			}
		} else {
			System.out.println("No hay Asignaturas almacenadas para mostrar.");
		}
		System.out.println("=============================\n");
		this.menuConsultarDatos();
	}
}